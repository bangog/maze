#pragma once
#include <SFML/Graphics.hpp>

#include"Scene.h"

class GameSceneGame :public Scene	
{
private:
	sf::View guiView;
	sf::View mainView;


public:
	GameSceneGame(Game * game);
	~GameSceneGame();
	
	//Nie Zaimpletowano
	void draw(float dt);
	void update(float dt);
	void imputHandler();
};

