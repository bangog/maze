#include "OverScreen.h"


OverScreen::OverScreen(Game *game,std::string endGameString)
{
	this->game = game;
	view.setSize(sf::Vector2f(game->window.getSize()));
	view.setCenter(sf::Vector2f(game->window.getSize())*0.5f);

	Texter::setText(mainText, sf::Color::White, game->mainFont, endGameString, 200);
	Texter::textCenter(mainText, game->window.getSize().x, game->window.getSize().y / 2);
}


OverScreen::~OverScreen()
{
}

void OverScreen::draw(const float dt)
{
	game->window.setView(this->view);
	game->window.clear(sf::Color::Black);
	game->window.draw(mainText);
	
}

void OverScreen::update(const float dt)
{

}


void OverScreen::inputHandler()
{
	sf::Event event;

	while (game->window.pollEvent(event))
	{
		switch (event.type)
		{


		case sf::Event::Closed:
			this->game->window.close();
			break;

			//Keyboard events
		case sf::Event::KeyReleased:
			switch (event.key.code)
			{

			case sf::Keyboard::Escape:
				this->game->window.close();
				break;

			default:
				break;
			}
			break;


		default:
			break;
		}
	}
}