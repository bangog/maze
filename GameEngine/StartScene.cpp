#include "StartScene.h"

StartScene::StartScene(Game * game)
{
	way = true;
	alpha = 5;
	this->game = game;
	alphaColor = new sf::Color(255, 0, 0, 180);

	view.setSize(sf::Vector2f(game->window.getSize()));
	view.setCenter(sf::Vector2f(game->window.getSize())*0.5f);

	Texter::setText(mainText, sf::Color::Green, game->mainFont, "Get MAZED", 150);
	Texter::textCenter(mainText, game->window.getSize().x, game->window.getSize().y/4 + game->window.getSize().y/10);

	Texter::setText(sideText, *alphaColor, game->mainFont, "Press S to start game or Esc to close !",80 );
	Texter::textCenter(sideText, game->window.getSize().x, game->window.getSize().y*1.4);

}


StartScene::~StartScene()
{
}

void StartScene::inputHandler()
{

	sf::Event event;

	while (game->window.pollEvent(event))
	{
		switch (event.type)
		{


		case sf::Event::Closed:
			this->game->window.close();
			break;

			//Keyboard events
			case sf::Event::KeyReleased:
			switch (event.key.code)
			{

			case sf::Keyboard::Escape:
				this->game->window.close();
				break;

			case sf::Keyboard::S:
				game->pushScene(new GameScene(game));
				

			default:
				break;
			}
			break;


		default:
			break;
		}
	}
}

void StartScene::update(float dt)
{
	if (way)
		alpha += 6;
	else
		alpha -= 14;

	if (alpha > 240)
		way = false;
	else if (alpha < 10)
		way = true;
	sf::Color color(255, 0, 0, alpha);
	sideText.setColor(color);
}

void StartScene::draw(float dt)
{
	game->window.setView(this->view);
	game->window.clear(sf::Color::White);
	game->window.draw(game->background);
	game->window.draw(mainText);
	game->window.draw(sideText);
}

