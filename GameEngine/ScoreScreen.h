#pragma once
#include "Scene.h"
#include "Game.h"

class ScoreScreen :public Scene
{
public:
	ScoreScreen(Game * game);
	~ScoreScreen();

	virtual void draw(const float dt);
	virtual void update(const float dt);
	virtual void imputHandler();
	Game* getGame();


private:
	Game * game;
	sf::View view; //Main camera
	sf::Text mainText;
	sf::Text sideText;
	float alpha;
	sf::Color * alphaColor;
	bool way;

};

