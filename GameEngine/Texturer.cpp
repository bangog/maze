#include "Texturer.h"

void Texturer::loadTexture(std::string id, std::string textureFilePath)
{
	sf::Texture newTexture;
	newTexture.loadFromFile(textureFilePath);
	textureMap[id] = newTexture;
	textureCount++;
}

sf::Texture& Texturer::getTextureFromID(std::string textureID)
{
	return textureMap.at(textureID); //We using .at instead of array like style becouse .at throws exeption if something went wrong
}

int Texturer::getTextureCount()
{
	return textureCount;
}

Texturer::Texturer()
{
	textureCount = 0;
}


