//piure virtual class used in Scenes
#pragma once
#include "Game.h"


class Scene
{
public:
	Game *game;

	virtual void draw(const float dt)=0;
	virtual void update(const float dt)=0;
	virtual void inputHandler()=0;

protected:

//	virtual void setText(sf::Font &font, sf::Text &text, std::string stringText, float x, float y);

};

