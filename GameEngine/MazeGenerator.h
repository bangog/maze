#pragma once
#define MAX 81  // 30 * 2 + 1
#define CELL 1900  // 30 * 30
#define WALL 1
#define PATH 0
#include<time.h>
#include<stdlib.h>
#include<iostream>
#include<fstream>
using namespace std;

class MazeGenerator
{
public:
	MazeGenerator(int size);
	~MazeGenerator();
	int** getMaze();

private:
	int **poi;
	int maze[MAX][MAX];
	int backtrack_x[CELL];
	int backtrack_y[CELL];
	int indeks;
	int size;


	void init_maze();
	void maze_generator(int indeks, int maze[MAX][MAX], int backtrack_x[CELL], int bactrack_y[CELL], int x, int y, int n, int visited);
	void print_maze(int maze[MAX][MAX], int maze_size);
	int is_closed(int maze[MAX][MAX], int x, int y);
};

