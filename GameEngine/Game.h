#pragma once
#include <stack>
#include <SFML/Graphics.hpp>
#include <iostream>
#include "Texturer.h"
#include "playerModel.h"


class Scene;

class Game
{
protected:
	void loadTexture(); //loads default texture called background
	void loadTexture(std::string texturePath); // loads texture from path and give it random name
	
public:
	std::stack<Scene*> sceneStack;
	sf::RenderWindow window;
	Texturer textureMenager;
	sf::Sprite background;
	sf::Font mainFont;
	PlayerModel mainPlayer;

	void loadTexture(std::string textureName, std::string texturePath); // load texture form path and give it proper name
	void pushScene(Scene* newScene);
	void popScene();
	void swapScene(Scene* newScene);
	Scene* peekScene();
	void gameLoop();

	Game();
	~Game();
};

