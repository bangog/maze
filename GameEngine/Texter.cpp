#include "Texter.h"

void Texter::textCenter(sf::Text &text, float width, float height)
{
	sf::FloatRect textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width / 2.0f,
		textRect.top + textRect.height / 2.0f);
	text.setPosition(sf::Vector2f(width / 2.0f, height / 2.0f));
}

void Texter::setText(sf::Text &text,sf::Color color, sf::Font &font, std::string textString, unsigned int size =30)
{
	text.setFont(font);
	text.setColor(color);
	text.setString(textString);
	text.setCharacterSize(size);
}