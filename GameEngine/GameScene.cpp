#include "GameScene.h"
#include "GameSceneGame.h"
#include "MazeGenerator.h"
#include "Scene.h"
#include "tile.h"


void GameScene::draw(float dt)
{
	game->window.setView(this->view);
	game->window.clear(sf::Color::White);
	game->window.draw(game->background);
	game->window.draw(*mazeTileMap);
	game->window.draw(*(game->mainPlayer.getPlayerSprite()));
	game->window.draw(timeText);
}

GameScene::GameScene(Game * game)
{
	//Setting view 
	this->game = game;
	sf::Vector2f pos = sf::Vector2f(this->game->window.getSize());
	this->view.setSize(pos);
	pos = pos*0.5f;
	this->view.setCenter(pos);


	//Setting text
	Texter::setText(timeText, sf::Color::Color(80, 255, 110, 255), game->mainFont, "123", 20);
	Texter::textCenter(timeText, 80, 25);
	
	//Random size of maze
	srand(time(NULL));
	int size = rand() % 9 + 2;
	int realSize = size * 2 + 1;

	game->mainPlayer.restart();
	game->mainPlayer.addTime(float(size+3));

	// Maze Generating
	MazeGenerator maze(size); // Creates Maze with 2*v + 1 length
	pointerToNewMaze = maze.getMaze(); // Gets pointer to Maze

	newmaze = new int[realSize*realSize];

	for (int a = 0; a < realSize; a++)
	{
		for (int b = 0; b < realSize; b++)
		{
			newmaze[b+realSize*a]= pointerToNewMaze[a][b];
			
		}
	}
	

	mazeTileMap = new TileMap;
	mazeTileMap->load("tiles.png", sf::Vector2u(32, 32), newmaze, realSize,realSize);
	delete newmaze;
	
}

void GameScene::update(float dt)
{
	//gameTime += dt;
	if (game->mainPlayer.removeTime(dt))
		loadScene("GAME OVER");
	else if (game->mainPlayer.getTime() > 60)
		loadScene("YOU WON !");
	timeText.setString("Time: " + std::to_string(game->mainPlayer.getTime()));
	//std::cout << "Time left: " << game->mainPlayer.getTime() << " sec" << std::endl;
}

void GameScene::inputHandler()
{
	sf::Event event;


	while (this->game->window.pollEvent(event))
	{

		switch (event.type)
		{

		case sf::Event::Closed:
			this->game->window.close();
			break;

		/*case sf::Event::Resized:
			this->view.setSize(event.size.width, event.size.height);
			
		
		this->game->background.setPosition(this->game->window.mapPixelToCoords(sf::Vector2i(0, 0)));
			this->game->background.setScale(float(event.size.width) / float(this->game->background.getTexture()->getSize().x), 
										     float(event.size.height) / float(this->game->background.getTexture()->getSize().y));
			break;*/

		case sf::Event::KeyPressed:
			switch (event.key.code)
			{

			case sf::Keyboard::Escape:
				this->game->window.close();
				break;

			case sf::Keyboard::Down:
				
				if (pointerToNewMaze[game->mainPlayer.y + 1][game->mainPlayer.x] == 0)
					game->mainPlayer.down();
				break;

			case sf::Keyboard::Up:
				if (pointerToNewMaze[game->mainPlayer.y - 1][game->mainPlayer.x] == 0)
					game->mainPlayer.up();
				break;

			case sf::Keyboard::Left:
				if (pointerToNewMaze[game->mainPlayer.y][game->mainPlayer.x - 1] == 0)
					game->mainPlayer.left();
				break;
				
				case sf::Keyboard::Right:
					if (pointerToNewMaze[game->mainPlayer.y][game->mainPlayer.x + 1] == 0)
						game->mainPlayer.right();

					if (pointerToNewMaze[game->mainPlayer.y][game->mainPlayer.x + 1] == 2)
						loadScene();

					break;
			default:
				break;
			}
			break;

		default:
			break;
		}
	}

}


GameScene::~GameScene()
{
	for (size_t i = 0; i < 82; i++)
	{
		delete pointerToNewMaze[i];
	}
	delete pointerToNewMaze;
}


void GameScene::loadScene()
{

	game->pushScene(new GameScene(game));
}

void GameScene::loadScene(std::string endScreenText)
{

	game->pushScene(new OverScreen(game, endScreenText));
}