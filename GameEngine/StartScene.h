#pragma once
#include "Scene.h"
#include "Game.h"
#include "SFML\Graphics.hpp"
#include "SFML\Audio.hpp"
#include "GameScene.h"
#include "Texter.h"
#include <vector>


class StartScene :public Scene
{
public:
	StartScene(Game * game);
	~StartScene();

	virtual void draw(const float dt);
	virtual void update(const float dt);
	virtual void inputHandler();
	Game* getGame();


private:
	Game * game;
	sf::View view; //Main camera
	sf::Text mainText;
	sf::Text sideText;
	float alpha;
	sf::Color * alphaColor;
	bool way;

};

