#include "playerModel.h"

bool PlayerModel::removeTime(float dt)
{
	time -= dt;

	if (time < 0)
		return true;
	else
		return false;
}

void PlayerModel::addTime(float dt)
{
	time += dt;
}

float PlayerModel::getTime()
{
	return time;
}


sf::Sprite * PlayerModel::getPlayerSprite()
{
	return &playerSprite;
}

void PlayerModel::left()
{
	playerSprite.move(-32, 0);
	x--;

}

void PlayerModel::right()
{
	playerSprite.move(32, 0);
	x++;
}

void PlayerModel::up()
{
	playerSprite.move(0, -32);
	y--;

}

void PlayerModel::down()
{
	playerSprite.move(0, 32);
	y++;
}

bool PlayerModel::restart()
{

	x = 0;
	y = 1;
	playerSprite.setPosition(0 * 32, 1 * 32);
	return true;
}

PlayerModel::PlayerModel()
{
	x = 1;
	y = 0;
	playerTexture.loadFromFile("player.png");
	playerSprite.setTexture(playerTexture);
	time = 10.0f;
}


PlayerModel::~PlayerModel()
{
}
