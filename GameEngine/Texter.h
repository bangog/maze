#pragma once
#include "SFML\Graphics.hpp"
#include <String>
class Texter
{
public:
	static void setText(sf::Text &text,sf::Color color, sf::Font &font, std::string textString, unsigned int size);
	static void textCenter(sf::Text &text, float width, float height);
};

