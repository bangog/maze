#pragma once
#include<map>
#include<SFML\Graphics.hpp>
#include<string>


/////////////////////////////////////////////
// Texturer is a class for Texture menagment
////////////////////////////////////////////
class Texturer
{
protected:
	std::map<std::string, sf::Texture> textureMap;
	int textureCount;

public:
	void loadTexture(std::string textureName, std::string textureFileName);
	sf::Texture& getTextureFromID(std::string textureName);
	int getTextureCount();

	Texturer();

};

