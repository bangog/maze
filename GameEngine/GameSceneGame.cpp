#include "GameSceneGame.h"

void GameSceneGame::update(float dt)
{

}

void GameSceneGame::draw(float dt)
{
	game->window.clear(sf::Color::Cyan);
	game->window.draw(game->background);
}

void GameSceneGame::imputHandler() //zarzadzanie eventami (nacisniecie kalwisza ect)
{
	sf::Event event;

	while (game->window.pollEvent(event))
	{


		switch (event.type)
		{


		case sf::Event::Resized:
			mainView.setSize(event.size.width, event.size.height);
			guiView.setSize(event.size.width, event.size.height);
			game->background.setPosition(game->window.mapPixelToCoords(sf::Vector2i(0, 0), this->guiView));
			game->background.setScale(float(event.size.width) / float(game->background.getTexture()->getSize().x),
				float(event.size.height) /  float(game->background.getTexture()->getSize().y));

			break;

		case sf::Event::Closed:
			game->window.close(); 
			break;


		case sf::Event::KeyPressed:
			/*if (event.key.code == sf::Keyboard::Escape)
				game->window.close();
			else*/ if (event.key.code == sf::Keyboard::BackSpace)
				game->window.close();
			break;


		default:
			break;
		}
	}
}

GameSceneGame::GameSceneGame(Game* game)
{
	this->game = game;
	sf::Vector2f size = sf::Vector2f(game->window.getSize());
	guiView.setSize(size);
	guiView.setCenter((size*0.5f));
	mainView.setSize(size);
	mainView.setCenter((size*0.5f));


}


GameSceneGame::~GameSceneGame()
{
}


