#pragma once
#include "TileMap.h"
#include "Scene.h"
#include "GameSceneGame.h"
#include "MazeGenerator.h"
#include "Scene.h"
#include "tile.h"
#include <time.h>
#include "Texter.h"
#include "OverScreen.h"

class GameScene : public Scene
{
private:
	sf::View view; // Glowna kamera menu
	void loadScene();
	void loadScene(std::string endScreenText);

	int * sizeHolder;
	int * newmaze;
	int ** pointerToNewMaze;
	double gameTime;
	int posx;
	int posy;

public:
	GameScene(Game * game);
	~GameScene();

	virtual void draw(float dt);
	virtual void update(float dt);
	virtual void inputHandler();
	
	TileMap* mazeTileMap;

	sf::Sprite playerSpirte;
	sf::Texture playerTexture;
	sf::Text timeText;
};

