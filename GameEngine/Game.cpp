
#include "Scene.h"

void Game::popScene()
{
	delete sceneStack.top();
	sceneStack.pop();
}

void Game::pushScene(Scene * newScene)
{
	sceneStack.push(newScene);
}

void Game::swapScene(Scene* newScene)
{
	
	popScene();
	pushScene(newScene);
}

Scene* Game::peekScene()
{
	if (this->sceneStack.empty())
		return nullptr;
	else
		return this->sceneStack.top();
}

void Game::gameLoop()
{
	int size;
	bool dupa = true;
	sf::Clock clock;
	std::cout << "??";
	while (this->window.isOpen())
	{
		sf::Time elapsed;
		elapsed=clock.restart();
		float dtime = elapsed.asSeconds();
		if (peekScene() == nullptr)	
			continue;
		
		peekScene()->inputHandler();
		peekScene()->update(dtime);
		peekScene()->draw(dtime);
		this->window.display();

	}

}

void Game::loadTexture()
{
	textureMenager.loadTexture("background", "background.png");
}

void Game::loadTexture(std::string filePath)
{
	textureMenager.loadTexture("texture" + textureMenager.getTextureCount(), filePath);
}

void Game::loadTexture(std::string textureName, std::string filePath)
{
	textureMenager.loadTexture(textureName, filePath);
}

Game::Game()
{
	loadTexture();
	window.create(sf::VideoMode(1366-20, 768-100), "Get Mazed");
	window.setFramerateLimit(60);
	background.setTexture(textureMenager.getTextureFromID("background"));
	mainFont.loadFromFile("Ubuntu-C.ttf");
	//newPlayer = new Player(this);
}


Game::~Game()
{
	for (size_t i = 0; i < sceneStack.size(); i++)
	{
		sceneStack.pop();
	}
}


