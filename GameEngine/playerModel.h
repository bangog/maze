#pragma once
#include "SFML\Graphics.hpp"
class PlayerModel
{
public:
	int x, y;
	PlayerModel();
	~PlayerModel();
	void left();
	void right();
	void up();
	void down();
	bool restart();


	void addTime(float dt);
	bool removeTime(float dt);
	float getTime();
	sf::Sprite* getPlayerSprite();

private:
	float time;
	sf::Texture playerTexture;
	sf::Sprite playerSprite;
};

